This is a highly flexible and easy extendable filter module to embed any type of social post in your site using a simple tag. 

========= Installation =========

Enable the module on the modules page.

Go to admin/config/content/formats and configure the text format(s) that should be allowed to use this filter. Check the box to enable Social Embed and save.
Some simple settings are available if you configure the text format. 

Make sure that Social Embed is processed before "Convert URLs to links".
You can do this by dragging and dropping Social Embed to the top of the processing order list. Do this even if it's allready on top, just to make sure!

If you're using the "Limit allowed HTML tags" filter, make sure Social Embed is processed after that filter.

To enable WYSIWYG support, go to the WYSIWYG settings for each input format and enable the Social Embed button.

========= Usage =========

Single social post: [social_embed:ID platform:platformName]
This will output the social post using the default settings.

You can also set some parameters in the call:
[social_embed:99530515043983360 platform:twitter tw_hide_media:1]
This will override the default twitter hide media for this post.


========= Add Social Network =========
All the social network supported by the module should be configured in social_embed.platforms.inc
Please read the following documentation about the code configuration standard

- social_embed_platform_info()
Add an array value for the new social network. 
EG. we choose to create a new social network called Tourtools Social.
Minimum configuration as follow

$platforms['tourtools'] = array(
  'name'          => t('Tourtools Social Network'),
  'settingsPrefix'=> 'ts_',
  'addData'       => array(),
  'sample_id'     => '',
  'callback'      => 'social_embed_tourtools',
  'instructions'  => t(''),
  'settings'      => array(
    'ts_postUrl'    => array(
      'showAdmin'       => false,
      'showEditor'      => true,
      'default_value'   => '',
      'type'            => 'textfield',
      'title'           => t('Tourtools Post ID'),
      'description'     => t('')
    ),
    'ts_socialName'  => array(
      'showAdmin'       => false,
      'showEditor'      => true,
      'default_value'   => 'tourtools',
      'type'            => 'hidden',
    ),
  )
);

Please note the following data
. $platforms keys (tourtools) should be unique;
. settingsPrefix should be unique and end with _ char;
. addData can have inline or external js library to add for the social network. It's an array with the following values
	. code: the js external url OR the js code to set as inline
	. type: inline-js OR external-js
	. convertCode: array of placeholder that we should convert in the code params by global setting. Please check Facebook setting for the placeholder {APPID} as example
. callback is the function to add in the same file with the operation for rendering the post
. settings is an array with all the fields that you can set for the social network. Each setting should have the following spec
	. the key should be unique and begin with the settingsPrefix previously defined;
	. showAdmin and showEditor enable the print of this setting in the admin config page and in the WYSIWYG plugin
	Please even note that the setting should always have the following setting
	. settingsPrefix_postUrl and settingsPrefix_socialName. For both fields please check one of the default social network for the configuration.
		This setting are used internament for the WYSIWYG plugin
