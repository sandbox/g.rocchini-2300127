(function ($) {
  tinymce.create('tinymce.plugins.social_embed', {
    init : function(ed, url) {
      // Register commands
      ed.addCommand('mceSocialEmbed', function() {
        ed.windowManager.open({
          file : Drupal.settings.social_embed.url.wysiwyg_tinymce,
          width : 580,
          height : 480,
          inline : true,
          scrollbars : 1,
          popup_css : false
        }, {
          plugin_url : url
        });
      });

      // Register buttons
      ed.addButton('social_embed', {
        title : 'Social Embed',
        cmd : 'mceSocialEmbed',
        image : url + '/images/social_embed.png'
      });
    },

    getInfo : function() {
      return {
        longname : 'Social Embed',
        author : 'Tourtools',
        authorurl : 'http://www.tourtools.it',
        infourl : 'http://www.tourtools.it',
        version : tinymce.majorVersion + "." + tinymce.minorVersion
      };
    }
  });
  // Register plugin
  tinymce.PluginManager.add('social_embed', tinymce.plugins.social_embed);
})(jQuery);
