var social_embed_dialog = {};

(function ($) {
social_embed_dialog = {
  insert : function() {
    var shortCodeStr  = "";
    var socialParams  = new Array;
    var ed            = tinyMCEPopup.editor, e;
    tinyMCEPopup.restoreSelection();
    tinyMCEPopup.execCommand("mceBeginUndoLevel");


    //Read all the params
    var params = {};
    $('fieldset input, fieldset select, fieldset select, fieldset radio').each(function() {
      if($(this).attr('type') == "checkbox") {
        if($(this).is(':checked')) {
          params[$(this).attr('name')] = $(this).val();
        }
      } 
      if($(this).attr('type') == "radio") {
        if($(this).is(':checked')) {
          params[$(this).attr('name')] = $(this).val();
        }
      } else {
        if($(this).val() != "" && $(this).val() != "none") {
          params[$(this).attr('name')] = $(this).val();
        }
      }
    });


    //All the params key is of kind socialkey_nameofparams
    //I need to create my social array params with socialId as key
    for(var key in params){
      var keySplit  = key.split("_"); //split the key by _
      var socialId  = keySplit[0];
      var attrId    = (keySplit.slice(1)).join("_"); //EG. tw_hide_media give back hide_media
      var attrVal   = params[key];
      if(!(socialId in socialParams)){
        socialParams[socialId] = new Array;
      }
      socialParams[socialId][attrId] = attrVal;
    }

    //Now for each social network with postUrl defined I create the shortcode
    for(var socialId in socialParams){
      if(('postUrl' in socialParams[socialId])){
        if(socialParams[socialId]['postUrl'] != ''){
          shortCodeStr += "[social_embed:"+socialParams[socialId]['postUrl'];
          shortCodeStr += " platform:"+socialParams[socialId]['socialName'];

          for(var attrId in socialParams[socialId]){
            if(attrId != "postUrl" && attrId != "socialName"){
              if(socialParams[socialId][attrId] != ""){
                shortCodeStr += " "+socialId+"_"+attrId+":"+socialParams[socialId][attrId];
              }
            }
          }

          shortCodeStr += "]";
        }
      }
    }

    //check if i have some content to show
    if(shortCodeStr == ""){
      //Simple close the popup
      ed.execCommand('mceRepaint');
      tinyMCEPopup.execCommand("mceEndUndoLevel");
      tinyMCEPopup.close();
    } else {
      ed.execCommand('mceInsertContent', false, shortCodeStr);
    }

    tinyMCEPopup.execCommand("mceEndUndoLevel");
    tinyMCEPopup.close();
  }
};

Drupal.behaviors.social_embed_tinymce =  {
  attach: function(context, settings) {
    $('#edit-insert').click(function() {
      social_embed_dialog.insert();
    });

    $('#edit-cancel').click(function() {
      tinyMCEPopup.close();
    });
  }
}

})(jQuery);
