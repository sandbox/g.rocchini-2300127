/**
 * @file Plugin for inserting social post tags with social_embed
 */
(function ($) {
  CKEDITOR.plugins.add('social_embed', {
    requires : [],

    //Add the button and the command to the editor
    init: function(editor) {
      // Add Button
      editor.ui.addButton('social_embed', {
        label:    'Social Embed',
        command:  'social_embed',
        icon:     this.path + 'social_embed.png'
      });
      // Add Command
      editor.addCommand('social_embed', {
        exec : function () {
          var path  = (Drupal.settings.social_embed.url.wysiwyg_ckeditor) ? Drupal.settings.social_embed.url.wysiwyg_ckeditor : Drupal.settings.social_embed.url.ckeditor
          var media = window.showModalDialog(path, { 'opener' : window, 'editorname' : editor.name }, "dialogWidth:700px; dialogHeight:550px; center:yes; resizable:yes; help:no;");
        }
      });

      // Register an extra fucntion, this will be used in the popup.
      editor._.social_embedFnNum = CKEDITOR.tools.addFunction(insert, editor);
    }
  });

  function insert(params, editor) {
    var socialParams  = new Array;
    var selection     = editor.getSelection();
    var ranges        = selection.getRanges();
    var shortCodeStr  = "";
    var range;
    var textNode;

    //Save the current state
    editor.fire('saveSnapshot');
    
    //All the params key is of kind socialkey_nameofparams
    //I need to create my social array params with socialId as key
    for(var key in params){
      var keySplit  = key.split("_"); //split the key by _
      var socialId  = keySplit[0];
      var attrId    = (keySplit.slice(1)).join("_"); //EG. tw_hide_media give back hide_media
      var attrVal   = params[key];
      if(!(socialId in socialParams)){
        socialParams[socialId] = new Array;
      }
      socialParams[socialId][attrId] = attrVal;
    }

    //Now for each social network with postUrl defined I create the shortcode
    for(var socialId in socialParams){
      if(('postUrl' in socialParams[socialId])){
        if(socialParams[socialId]['postUrl'] != ''){
          shortCodeStr += "[social_embed:"+socialParams[socialId]['postUrl'];
          shortCodeStr += " platform:"+socialParams[socialId]['socialName'];

          for(var attrId in socialParams[socialId]){
            if(attrId != "postUrl" && attrId != "socialName"){
              if(socialParams[socialId][attrId] != ""){
                shortCodeStr += " "+socialId+"_"+attrId+":"+socialParams[socialId][attrId];
              }
            }
          }

          shortCodeStr += "]";
        }
      }
    }

    for (var i = 0, len = ranges.length; i < len; i++) {
      range = ranges[i];
      range.deleteContents();

      textNode = CKEDITOR.dom.element.createFromHtml(shortCodeStr);
      range.insertNode(textNode);
    }

    range.moveToPosition(textNode, CKEDITOR.POSITION_AFTER_END);
    range.select();

    editor.fire('saveSnapshot');
  }

})(jQuery);
