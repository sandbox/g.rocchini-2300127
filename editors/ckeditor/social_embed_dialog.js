/**
 * @file social_embed ckeditor dialog helper
 */

var social_embed_dialog = {};
(function ($) {

  //Dialog element with custom init and functions
  social_embed_dialog = {
    //In the init I get the current editor instance
    init : function() {
      CKEDITOR  = dialogArguments.opener.CKEDITOR; //Get CKEDITOR
      var name  = dialogArguments.editorname; //Get the current instance name
      editor    = CKEDITOR.instances[name]; //Get the editor instance
    },

    //In the insert function I get the form params and call the plugin.js function to compose the text
    insert : function() {
      var params  = this._getParams();
      CKEDITOR.tools.callFunction(editor._.social_embedFnNum, params, editor);
      window.close();
    },

    //Function to get all the params from the form
    _getParams : function () {
      var params = {};
      $('fieldset input, fieldset select, fieldset select, fieldset radio').each(function() {
        if($(this).attr('type') == "checkbox") {
          if($(this).is(':checked')) {
            params[$(this).attr('name')] = $(this).val();
          }
        } 
        if($(this).attr('type') == "radio") {
          if($(this).is(':checked')) {
            params[$(this).attr('name')] = $(this).val();
          }
        } else {
          if($(this).val() != "" && $(this).val() != "none") {
            params[$(this).attr('name')] = $(this).val();
          }
        }
      });

      return params;
    }
  };

  //I create custom function for the button edit-insert and edit-cancel, removing the control from the default Drupal form system
  $(document).ready(function() {
    var CKEDITOR, editor;
    social_embed_dialog.init();

    $('#edit-insert').click(function() {
      social_embed_dialog.insert();
      return false;
    });

    $('#edit-cancel').click(function() {
      window.close();
      return false;
    });
  });
})(jQuery);
