<?php
/**
 * @file
 * This file contains all the social platform provided by Social Embed.
 */

/**
 * Implements hook_platform_info().
 */
function social_embed_platform_info() {
  $platforms = array();

  $platforms['twitter'] = array(
    'name'          => t('Twitter'),
    'settingsPrefix'=> 'tw_',
    'addData'       => array(
      0               => array(
        'code'          => 'http://platform.twitter.com/widgets.js',
        'type'          => 'external-js'
      )
    ),
    'sample_id'     => '99530515043983360',
    'callback'      => 'social_embed_twitter',
    'instructions'  => t('You must use the ID of the tweet and add the tag platform:tweeter. You can also set the following tag') .
      '<ol>' .
      '<li>' . t('tw_maxwidth, value between 250 and 550') . '</li>' .
      '<li>' . t('tw_hide_media, value true or false') . '</li>' .
      '<li>' . t('tw_hide_thread, value true or false') . '</li>' .
      '<li>' . t('tw_align, value left, right, center, none') . '</li>' .
      '</li>' .
      '</ol>',
    'settings'      => array(
      'tw_postUrl'    => array(
        'showAdmin'       => false,
        'showEditor'      => true,
        'default_value'   => '',
        'type'            => 'textfield',
        'title'           => t('Tweet ID'),
        'description'     => t('You must use the ID of the tweet (e.g. 99530515043983360)')
      ),
      'tw_socialName'  => array(
        'showAdmin'       => false,
        'showEditor'      => true,
        'default_value'   => 'twitter',
        'type'            => 'hidden',
      ),
      'tw_maxwidth'   => array(
        'showAdmin'       => true,
        'showEditor'      => true,
        'default_value'   => 550,
        'type'            => 'textfield',
        'title'           => t('Default Twitter maxwidth'),
        'description'     => t('The maximum width in pixels that the embed should be rendered at. This value is constrained to be between 250 and 550.')
      ),
      'tw_align'      => array(
        'showAdmin'       => true,
        'showEditor'      => true,
        'default_value'   => 'none',
        'type'            => 'select',
        'title'           => t('Default Twitter align'),
        'description'     => t('Specifies whether the embedded Tweet should be left aligned, right aligned, or centered in the page.'),
        'options'         => array(
          'none'            => t('None'),
          'left'            => t('Left'),
          'center'          => t('Center'),
          'right'           => t('Right')
        )
      ),
      'tw_hide_media' => array(
        'showAdmin'       => true,
        'showEditor'      => true,
        'default_value'   => 0,
        'type'            => 'radios',
        'title'           => t('Default Twitter hide media'),
        'description'     => t('Specifies whether the embedded Tweet should automatically expand images which were uploaded via POST statuses/update_with_media.'),
        'options'         => array(
          0 => t('Images will be expanded'),
          1 => t('Images will not be expanded')
        )
      ),
      'tw_hide_thread'=> array(
        'showAdmin'       => true,
        'showEditor'      => true,
        'default_value'   => 0,
        'type'            => 'radios',
        'title'           => t('Default Twitter hide thread'),
        'description'     => t('Specifies whether the embedded Tweet should automatically show the original message in the case that the embedded Tweet is a reply.'),
        'options'         => array(
          0 => t('The original Tweet will be shown'),
          1 => t('The original Tweet will not be shown')
        )
      )
    )
  );

  $platforms['google+'] = array(
    'name'          => t('Google+'),
    'settingsPrefix'=> 'g+_',
    'addData'       => array(
      0               => array(
        'code'          => 'https://apis.google.com/js/plusone.js',
        'type'          => 'external-js'
      )
    ),
    'sample_id'     => '118109321735927344805/posts/DhXTS4pRWiq',
    'callback'      => 'social_embed_googleplus',
    'instructions'  => t('You must use the complete ID of the post and add the tag platform:google+. This module has not additional configuration'),
    'settings'      => array(
      'g+_postUrl'    => array(
        'showAdmin'       => false,
        'showEditor'      => true,
        'default_value'   => '',
        'type'            => 'textfield',
        'title'           => t('Google+ ID'),
        'description'     => t('You must use the complete ID of the post (e.g. 118109321735927344805/posts/DhXTS4pRWiq)')
      ),
      'g+_socialName'  => array(
        'showAdmin'       => false,
        'showEditor'      => true,
        'default_value'   => 'google+',
        'type'            => 'hidden',
      ),
    )
  );

  //Facebook request to add an inline javascript to convert the code.
  //Please note that it request the APPID value that here is between {}
  //The Facebook library create by himself the div with if fb-root
  $platforms['facebook'] = array(
    'name'          => t('Facebook'),
    'settingsPrefix'=> 'fb_',
    'addData'       => array(
      1               => array(
        'code'          => '(function(d, s, id) {
                              var js, fjs = d.getElementsByTagName(s)[0];
                              if (d.getElementById(id)) return;
                              js = d.createElement(s); js.id = id;
                              js.src = "//connect.facebook.net/it_IT/sdk.js#xfbml=1&appId={APPID}&version=v2.0";
                              fjs.parentNode.insertBefore(js, fjs);
                            }(document, \'script\', \'facebook-jssdk\'));',
        'type'          => 'inline-js',
        'convertCode'   => array(
          0               => array(
            'search'        => '{APPID}',
            'replaceAttr'   => 'fb_APPID'
          )
        )
      )
    ),
    'sample_id'     => 'permalink.php?story_fbid=1499964140236828&amp;id=1414548045445105',
    'callback'      => 'social_embed_facebook',
    'instructions'  => t('You must use the complete URL of the post (without https://www.facebook.com/) and add the tag platform:facebook. You can also set the following tag') .
      '<ol>' .
      '<li>' . t('fb_width, value between 350 and 750') . '</li>' .
      '</li>' .
      '</ol>',
    'settings'      => array(
      'fb_postUrl'    => array(
        'showAdmin'       => false,
        'showEditor'      => true,
        'default_value'   => '',
        'type'            => 'textfield',
        'title'           => t('Facebook URL'),
        'description'     => t('You must use the complete URL of the post (e.g. tourtools/posts/10152584460538081)')
      ),
      'fb_socialName'  => array(
        'showAdmin'       => false,
        'showEditor'      => true,
        'default_value'   => 'facebook',
        'type'            => 'hidden',
      ),
      'fb_width'      => array(
        'showAdmin'       => true,
        'showEditor'      => true,
        'default_value'   => 500,
        'type'            => 'textfield',
        'title'           => t('Default Facebook post width'),
        'description'     => t('The maximum width in pixels that the embed should be rendered at. This value is constrained to be between 350 and 750.')
      ),
      'fb_APPID'      => array(
        'showAdmin'       => true,
        'showEditor'      => false,
        'default_value'   => '',
        'type'            => 'textfield',
        'title'           => t('Default Facebook App Id for sharing'),
        'description'     => t('You should create an App before use this plugin. Please insert here the App Id. Is not possible change this value in the shortcode')
      )
    )
  );

  return $platforms;
}

/**
 * Callback for Twitter platforms.
 *
 * @see social_embed_platform_info()
 */
function social_embed_twitter($socialPost) {
  //Set the endPoint URL for the oEmbed call
  $endPoint   = 'https://api.twitter.com/1/statuses/oembed.json?omit_script=true&id='.$socialPost['id']."&";

  //Prepare the attributes for calling the Twitter endpoint
  foreach($socialPost['params'] as $paramId=>$paramVal){
    $endPoint .= $paramId."=".$paramVal."&";
  }

  //Remove the last & from the url
  $finalUrl   = substr($endPoint, 0, -1);

  //Make the call with the custom function and get back the HTML returned from Twitter
  $data       = social_embed_oembed_request($finalUrl, array());
  $html       = "<div class='social_embed_twitter'>".$data['html']."</div>";

  //Return the HTML
  return $html;
}

/**
 * Callback for Google+ platforms.
 * Google+ embed request to create a div with custom class and data-href.
 * 
 * @see social_embed_platform_info()
 */
function social_embed_googleplus($socialPost) {
  //Create the html content and return it
  $html = '<div class="social_embed_google+"><div class="g-post" data-href="https://plus.google.com/'.$socialPost['id'].'"></div></div>';

  //Return the HTML
  return $html;
}

/**
 * Callback for Facebook platforms.
 * Facebook embed request to create a div with custom class and data-href.
 * 
 * @see social_embed_platform_info()
 */
function social_embed_facebook($socialPost) {
  //Create the html content and return it
  $html = '<div class="social_embed_facebook">
            <div class="fb-post" data-href="https://www.facebook.com/'.$socialPost['id'].'" data-width="'.$socialPost['params']['width'].'">
              <div class="fb-xfbml-parse-ignore"></div>
            </div>
           </div>';

  //Return the HTML
  return $html;
}